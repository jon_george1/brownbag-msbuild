namespace DemoApp.Models
{
    public class ImportDuty : Tax
    {
        protected override decimal CalculateTax(decimal price)
        {
            return price * 0.05m;
        }
    }
}