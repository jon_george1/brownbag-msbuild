namespace DemoApp.Models
{
    using System;

    public abstract class Tax
    {
        public decimal GetTaxAmount(decimal price)
        {
            return RoundTax(this.CalculateTax(price));
        }

        protected abstract decimal CalculateTax(decimal price);

        private static decimal RoundTax(decimal tax)
        {
            return Math.Round(tax * 20) / 20;
        }
    }
}