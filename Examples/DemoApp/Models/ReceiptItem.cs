namespace DemoApp.Models
{
    public class ReceiptItem
    {
        public string Name { get; set; }

        public decimal NetPrice { get; set; }

        public decimal Taxes { get; set; }

        public decimal TotalPrice { get; set; }
    }
}