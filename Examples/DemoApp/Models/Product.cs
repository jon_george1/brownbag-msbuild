﻿namespace DemoApp.Models
{
    using System.Collections.Generic;
    using System.Linq;

    public class Product
    {
        private readonly List<Tax> taxes;

        public Product(int id, string name, decimal price, IEnumerable<Tax> taxes)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.taxes = taxes.ToList();
        }

        public int Id { get; private set; }

        public string Name { get; private set; }

        public decimal Price { get; private set; }

        public decimal GetTax()
        {
            return this.taxes.Sum(x => x.GetTaxAmount(this.Price));
        }

        public decimal GetTotalPrice()
        {
            return this.Price + this.GetTax();
        }
    }
}