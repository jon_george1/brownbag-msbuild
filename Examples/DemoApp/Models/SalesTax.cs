namespace DemoApp.Models
{
    public class SalesTax : Tax
    {
        protected override decimal CalculateTax(decimal price)
        {
            return price * 0.1m;
        }
    }
}