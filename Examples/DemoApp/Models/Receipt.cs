﻿namespace DemoApp.Models
{
    using System.Collections.Generic;
    using System.Linq;

    public class Receipt
    {
        private readonly List<ReceiptItem> receiptItems;

        public Receipt(IEnumerable<Product> products)
        {
            var productsList = products.ToList();
            this.receiptItems = productsList.Select(x => new ReceiptItem { Name = x.Name, NetPrice = x.Price, Taxes = x.GetTax(), TotalPrice = x.GetTotalPrice() }).ToList();
            this.SalesTaxes = productsList.Sum(x => x.GetTax());
            this.TotalPrice = this.receiptItems.Sum(x => x.TotalPrice);
        }

        public IEnumerable<ReceiptItem> ReceiptItems
        {
            get
            {
                return this.receiptItems;
            }
        } 

        public decimal TotalPrice { get; private set; }

        public decimal SalesTaxes { get; private set; }
    }
}