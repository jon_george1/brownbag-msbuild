﻿namespace DemoApp.Controllers
{
    using System.Web.Mvc;

    using DemoApp.Infrastructure;

    public class HomeController : Controller
    {
        private ProductRepository productRepository;

        public HomeController()
        {
            this.productRepository = new ProductRepository();
        }

        public ActionResult Index()
        {
            this.ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            this.ViewBag.Products = this.productRepository.All();

            return this.View();
        }
    }
}