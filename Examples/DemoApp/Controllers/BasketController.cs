﻿namespace DemoApp.Controllers
{
    using System.Linq;
    using System.Web.Mvc;

    using DemoApp.Infrastructure;
    using DemoApp.Models;

    public class BasketController : Controller
    {
        private readonly ProductRepository productRepository;

        public BasketController()
        {
            this.productRepository = new ProductRepository();
        }

        [HttpGet]
        public ActionResult Index()
        {
            var basket = Basket.Current;
            var receipt = new Receipt(basket);

            return this.View(receipt);
        }

        [HttpPost]
        public ActionResult Add(int productId)
        {
            var product = this.productRepository.ById(productId);
            Basket.Current.Add(product);

            return this.Redirect(this.HttpContext.Request.UrlReferrer.ToString());
        }

        [HttpPost]
        public ActionResult Empty()
        {
             Basket.Current.Clear();

            return this.Redirect(this.HttpContext.Request.UrlReferrer.ToString());
        }

        [HttpGet]
        public ViewResult Summary()
        {
            var basket = Basket.Current;

            this.ViewBag.ItemCount = basket.Count;
            this.ViewBag.TotalPrice = basket.Sum(x => x.GetTotalPrice());

            return this.View();
        }
    }
}