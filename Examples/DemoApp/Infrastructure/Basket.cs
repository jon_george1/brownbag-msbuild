﻿namespace DemoApp.Infrastructure
{
    using System.Collections.ObjectModel;
    using System.Web;

    using DemoApp.Models;

    public class Basket : Collection<Product>
    {
         public static Basket Current 
         {
             get
             {
                 if (HttpContext.Current.Session["basket"] == null)
                 {
                     HttpContext.Current.Session["basket"] = new Basket();
                 }

                 return (Basket)HttpContext.Current.Session["basket"];
             }
         }
    }
}