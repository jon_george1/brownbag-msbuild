﻿namespace DemoApp.Infrastructure
{
    using System.Collections.Generic;
    using System.Linq;

    using DemoApp.Models;

    public class ProductRepository
    {
        private static readonly Tax[] ImportDuty = new[] { new ImportDuty() };

        private static readonly Tax[] NoTax = new Tax[0];

        private static readonly Tax[] SalesAndImportDuty = new Tax[] { new SalesTax(), new ImportDuty() };

        private static readonly Tax[] SalesTax = new[] { new SalesTax() };

        private static readonly Product[] Data = new[]
            {
                new Product(1, "Book", 12.49m, NoTax), 
                new Product(2, "CD", 14.99m, SalesTax), 
                new Product(3, "Chocolate bar", 0.85m, NoTax), 
                new Product(4, "Imported box of chocolates", 10.00m, ImportDuty), 
                new Product(5, "Imported bottle of perfume", 47.50m, SalesAndImportDuty), 
                new Product(6, "Imported bottle of perfume", 27.99m, SalesAndImportDuty), 
                new Product(7, "Bottle of perfume", 18.99m, SalesTax), 
                new Product(8, "Packet of headache pills", 9.75m, NoTax), 
                new Product(9, "Imported box of chocolates", 11.25m, ImportDuty)
            };

        public IEnumerable<Product> All()
        {
            return Data;
        }

        public Product ById(int productId)
        {
            return Data.First(x => x.Id == productId);
        }
    }
}