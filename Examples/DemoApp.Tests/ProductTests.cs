﻿namespace DemoApp.Tests
{
    using DemoApp.Models;

    using NUnit.Framework;

    [TestFixture]
    public class ProductTests
    {
        [Test]
        public void ProductWithASingleTaxCalculatesTaxCorrectly()
        {
            var product = new Product(1, "Test", 48.72m, new[] { new SalesTax() });

            var result = product.GetTax();

            Assert.That(result, Is.EqualTo(4.85m));
        }

        [Test]
        public void ProductWithASingleTaxCalculatesPriceCorrectly()
        {
            var product = new Product(1, "Test", 48.72m, new[] { new SalesTax() });

            var result = product.GetTotalPrice();

            Assert.That(result, Is.EqualTo(53.57m));
        }

        [Test]
        public void ProductWithNoTaxesCalculatesTaxCorrectly()
        {
            var product = new Product(1, "Test", 48.72m, new Tax[0]);

            var result = product.GetTax();

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void ProductWithNoTaxesCalculatesTotalPriceCorrectly()
        {
            var product = new Product(1, "Test", 48.72m, new Tax[0]);

            var result = product.GetTotalPrice();

            Assert.That(result, Is.EqualTo(48.72));
        }

        [Test]
        public void ProductWithMultipleTaxesCalculatesTaxCorrectly()
        {
            var product = new Product(1, "Test", 54.65m, new Tax[] { new SalesTax(), new ImportDuty() });

            var result = product.GetTax();

            Assert.That(result, Is.EqualTo(8.20m));
        }

        [Test]
        public void ProductWithMultipleTaxesCalculatesPriceCorrectly()
        {
            var product = new Product(1, "Test", 54.65m, new Tax[] { new SalesTax(), new ImportDuty() });

            var result = product.GetTotalPrice();

            Assert.That(result, Is.EqualTo(62.85m));
        }
    }
}