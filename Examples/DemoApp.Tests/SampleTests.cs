﻿namespace DemoApp.Tests
{
    using System.Linq;

    using DemoApp.Models;

    using NUnit.Framework;

    [TestFixture]
    public class SampleTests
    {
        [Test]
        public void Scenario1()
        {
            var products = TestProducts.Scenario1.All;

            var receipt = new Receipt(products);
            var items = receipt.ReceiptItems.ToList();

            Assert.That(items[0].TotalPrice, Is.EqualTo(12.49m));
            Assert.That(items[1].TotalPrice, Is.EqualTo(16.49m));
            Assert.That(items[2].TotalPrice, Is.EqualTo(0.85m));

            Assert.That(receipt.SalesTaxes, Is.EqualTo(1.5m));
            Assert.That(receipt.TotalPrice, Is.EqualTo(29.83m));
        }

        [Test]
        public void Scenario2()
        {
            var products = TestProducts.Scenario2.All;

            var receipt = new Receipt(products);
            var items = receipt.ReceiptItems.ToList();

            Assert.That(items[0].TotalPrice, Is.EqualTo(10.50m));
            Assert.That(items[1].TotalPrice, Is.EqualTo(54.65m));

            Assert.That(receipt.SalesTaxes, Is.EqualTo(7.65m));
            Assert.That(receipt.TotalPrice, Is.EqualTo(65.15m));
        }

        [Test]
        public void Scenario3()
        {
            var products = TestProducts.Scenario3.All;

            var receipt = new Receipt(products);
            var items = receipt.ReceiptItems.ToList();

            Assert.That(items[0].TotalPrice, Is.EqualTo(32.19m));
            Assert.That(items[1].TotalPrice, Is.EqualTo(20.89m));
            Assert.That(items[2].TotalPrice, Is.EqualTo(9.75m));
            Assert.That(items[3].TotalPrice, Is.EqualTo(11.8m));

            Assert.That(receipt.SalesTaxes, Is.EqualTo(6.65m));
            Assert.That(receipt.TotalPrice, Is.EqualTo(74.63m));
        }
    }
}