﻿namespace DemoApp.Tests
{
    using System.Linq;

    using DemoApp.Models;

    using NUnit.Framework;

    [TestFixture]
    public class ReceiptTests
    {
        [Test]
        public void ReceiptIsCorrectlyPopulatedWithItems()
        {
            var products = new[]
                {
                   TestProducts.Scenario1.Book, TestProducts.Scenario1.Cd, TestProducts.Scenario1.Chocolate 
                };

            var receipt = new Receipt(products);
            var receiptItems = receipt.ReceiptItems.ToList();

            Assert.That(receiptItems.Count, Is.EqualTo(3));

            for (var index = 0; index < 3; index++)
            {
                Assert.That(receiptItems[0].Name, Is.EqualTo(products[0].Name));
                Assert.That(receiptItems[0].TotalPrice, Is.EqualTo(products[0].GetTotalPrice()));
            }
        }

        [Test]
        public void ReceiptWithNoItemsCalculatesTotalsCorrectly()
        {
            var receipt = new Receipt(new Product[0]);

            Assert.That(receipt.SalesTaxes, Is.EqualTo(0));
            Assert.That(receipt.TotalPrice, Is.EqualTo(0));
        }

        [Test]
        public void ReceiptCorrectlyCalculatesTotalPrice()
        {
            var products = new[]
                {
                   TestProducts.Scenario1.Book, TestProducts.Scenario1.Cd, TestProducts.Scenario1.Chocolate 
                };

            var receipt = new Receipt(products);
            var expectedTotalPrice = products.Sum(x => x.GetTotalPrice());

            Assert.That(receipt.TotalPrice, Is.EqualTo(expectedTotalPrice));
        }

        [Test]
        public void ReceiptCorrectlyCalculatesTotalTaxes()
        {
            var products = new[]
                {
                   TestProducts.Scenario1.Book, TestProducts.Scenario1.Cd, TestProducts.Scenario1.Chocolate 
                };

            var receipt = new Receipt(products);
            var expectedTax = products.Sum(x => x.GetTax());

            Assert.That(receipt.SalesTaxes, Is.EqualTo(expectedTax));
        }
    }
}