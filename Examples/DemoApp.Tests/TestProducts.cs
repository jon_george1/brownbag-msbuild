﻿namespace DemoApp.Tests
{
    using DemoApp.Models;

    public static class TestProducts
    {
        public static Tax[] SalesTax = new[] { new SalesTax() };

        public static Tax[] ImportDuty = new[] { new ImportDuty() };

        public static Tax[] SalesAndImportDuty = new Tax[] { new SalesTax(), new ImportDuty() };

        public static Tax[] NoTax = new Tax[0];

        public static class Scenario1
        {
            public static Product Book = new Product(1, "Book", 12.49m, NoTax);
            public static Product Cd = new Product(2, "CD", 14.99m, SalesTax);
            public static Product Chocolate = new Product(3, "Chocolate bar", 0.85m, NoTax);

            public static Product[] All = new Product[] { Book, Cd, Chocolate };
        }

        public static class Scenario2
        {
            public static Product Chocolate = new Product(1, "Imported box of chocolates", 10.00m, ImportDuty);
            public static Product Perfume = new Product(2, "Imported bottle of perfume", 47.50m, SalesAndImportDuty);

            public static Product[] All = new Product[] { Chocolate, Perfume };
        }

        public static class Scenario3
        {
            public static Product ImportedPerfume = new Product(1, "Imported bottle of perfume", 27.99m, SalesAndImportDuty);
            public static Product Perfume = new Product(2, "Bottle of perfume", 18.99m, SalesTax);
            public static Product Pills = new Product(3, "Packet of headache pills", 9.75m, NoTax);
            public static Product Chocolate = new Product(4, "Imported box of chocolates", 11.25m, ImportDuty);

            public static Product[] All = new Product[] { ImportedPerfume, Perfume, Pills, Chocolate };
        }
    }
}